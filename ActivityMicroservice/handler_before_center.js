/**
 * Created by George on 18/03/2017.
 */

'use strict';

// dependencies
let async = require('async');
let moment = require('moment');
let uuid = require('node-uuid');
let _ = require('underscore');
let gm = require('gm').subClass({ imageMagick: true }); // Enable ImageMagick integration.
let util = require('util');
let rp = require('request-promise');
let P = require('bluebird');
let sleep = require('sleep-promise');
let PlacesClient = require('@google/maps').createClient({
    key: 'AIzaSyADoQ-UgT5yzEvMk6l44kek-wspbfXWg1Q',
    Promise: P
});

let localTemplates = require('./Templates.json');

// AWS
let AWS = require('aws-sdk');
AWS.config.update({region: 'eu-west-2'});
AWS.config.setPromisesDependency(P);

let dynamoDB = new AWS.DynamoDB.DocumentClient({apiVersion: '2012-08-10'});

let activityTemplate = '';
let activityDrink = '';
let userAllInfo = {};
let userLat;
let userLng;
let places = [];
let foundPlacesGoogle = {};
let placeName, placeId, placeLocation, placePhoto, placeRating;
let centerPoints = [{"lat":51.531062, "lng":-0.152611}, {"lat":51.530574, "lng":-0.066724}, {"lat":51.505811, "lng":-0.070679}, {"lat":51.507041, "lng":-0.157025}];

//Complete list of available parameters
const algParams = ["Adventure","Extreme","Football","Tennis","Indoor games","Rugby","Basketball","Volleyball","Golf","Jazz","Pop/Commercial","Rap","Trap","Rock","Classical","Country","Disco","EDM","Italian","British","Chinese","Thai","Indian","Vietnamese","French","Mexican","Greek","Japanese","Mediterranean","Moroccan","Turkish","Jamaican","Spanish","Reading","Cooking","Movies","Running","Cycling","Walking","Self-development","Charity","Camping","Board games","Video games","Clubbing","Photography","Shopping","Spontaneous","Flirty","Hungry","Relax","House Party","Energy Boost","Booze","Adrenaline High","Challenging","Normal"];

//Helpful constants
// number of km per degree = ~111km (111.32 in google maps, but range varies
// between 110.567km at the equator and 111.699km at the poles)
// 1km in degree = 1 / 111.32km = 0.0089
// 1m in degree = 0.0089 / 1000 = 0.0000089
const coordConst = 0.0000089;

const latChange = [0,0,0,500,-500];
const lngChange = [0,500,-500,0,0];

/**
 * Provide an event that contains the following keys:
 *
 * - resource: API Gateway resource for event
 * - path: path of the HTTPS request to the microservices API call
 * - httpMethod: HTTP method of the HTTPS request from microservices API call
 * - headers: HTTP headers for the HTTPS request from microservices API call
 * - queryStringParameters: query parameters of the HTTPS request from microservices API call
 * - pathParameters: path parameters of the HTTPS request from microservices API call
 * - stageVariables: API Gateway stage variables, if applicable
 * - body: body of the HTTPS request from the microservices API call
 */
module.exports.handler = function (event, context, callback) {
    console.log(event);
    activityTemplate = '';
    activityDrink = '';
    userAllInfo = {};
    let _response = "";
    let raincheckActivity = {};
    let inCenter = false;
    let invalid_path_err = {"Error": "Invalid path request " + event.resource + ', ' + event.httpMethod};

    let userId = event.queryStringParameters.userId;
    let userLocation = [];//event.queryStringParameters.location;
    userLat = parseFloat(event.queryStringParameters.lat);
    userLng = parseFloat(event.queryStringParameters.lng);
    userLocation.push(userLat);
    userLocation.push(userLng);
    let activityType = event.queryStringParameters.type;
    let mood = event.queryStringParameters.mood;
    let userTime = parseInt(event.queryStringParameters.time);

    //Deal with incoming request for an activity
    if (event.resource === '/activity' && event.httpMethod === "GET") {
        console.log("Generating new activity for user");

        let templates = [];

        //Collect all data about user
        let userInterests = new Array(105).fill(0);
        let userPreferences = {};

        let queryParams = {
            TableName: 'UserInfo',
            KeyConditionExpression: '#userId = :userid',
            ExpressionAttributeValues: {
                ':userid': 'key'
            },
            ExpressionAttributeNames: {
                "#userId": "UserID"
            }
        };

        let startTime, endTime;
        startTime = new Date();
        let searchPromiseArray = [];
        let foundActivity = false;
        foundPlacesGoogle = {};

        let busyWait = new Promise(function(resolve, reject) {
            setTimeout(function() {
              resolve();
            }, 3000);
        });

        searchPromiseArray.push(busyWait);

        queryParams["ExpressionAttributeValues"][':userid'] = userId; //Change to actual user on new algorithm
        let userInfoPromise = dynamoDB.query(queryParams).promise()
            .then(function(data){
                console.log("User info " + JSON.stringify(data));
                data.Items[0]["interests"].forEach (function (interest){
                    userInterests[interest] = 1;
                });
                console.log("User interests", userInterests);

                userPreferences = data.Items[0]["Preferences"];
                
                userAllInfo = data.Items[0];

                let raincheckIndex;

                if (userAllInfo.raincheck && userAllInfo.raincheck.length > 0) {
                    for (let index = 0; index < userAllInfo.raincheck.length; index++) {
                        if (userAllInfo.raincheck[index].deadline < new Date().getTime()) {
                            foundActivity = true;
                            raincheckIndex = index;
                            raincheckActivity = userAllInfo.raincheck[index].activity;
                            break;
                        }
                    }
                    if (foundActivity) {
                        userAllInfo.raincheck.splice(raincheckIndex,1);
                    }
                }
                if (!foundActivity) {
                    //Filter metadata places on Name, matching with the results from Google Places
                    queryParams = {
                        TableName: 'Metadata',
                        KeyConditionExpression: '#name = :placeName',
                        ExpressionAttributeValues: {
                            ':placeName': 'key'
                        },
                        ExpressionAttributeNames: {
                            "#name": "Name"
                        }
                    };

                    //Parameters for Google Places search
                    let query = {
                        location: userLocation,
                        language: "en",
                        rankby: "distance",
                        type: "bar",
                        opennow: true
                    };

                    if (userLocation[0] > centerPoints[3].lat && userLocation[0] < centerPoints[1].lat && userLocation[1] > centerPoints[3].lng && userLocation[1] < centerPoints[1].lng) {
                        console.log("User in zone 1");
                        inCenter = true;
                    }

                    // else {
                    //     //Artificially change user location by 500m N/S/W/E to cover more places
                    //     for (let i = 0; i < latChange.length; i++) {
                    //         let coefLat = latChange[i] * coordConst;
                    //         let coefLng = lngChange[i] * coordConst;

                    //         let new_lat = userLocation[0] + coefLat;
                    //         // pi / 180 = 0.018
                    //         let new_long = userLocation[1] + coefLng / Math.cos(userLocation[0] * 0.018);

                    //         query.location = [new_lat, new_long];

                    //         let searchPromise = PlacesClient.placesNearby(query).asPromise()
                    //             .then(function(response) {
                    //                 for (let i = 0; i < response.json.results.length; i++) {
                    //                     let addPlace = true;
                                        
                    //                     if (userAllInfo.restrictedPlaces) {
                    //                         for (let j = 0; j < userAllInfo.restrictedPlaces.length; j++) {
                    //                             console.log("Restricted place", userAllInfo.restrictedPlaces[j]);
                    //                             if (typeof userAllInfo.restrictedPlaces[j].deadline === 'string') {
                    //                                 if (response.json.results[i].name === userAllInfo.restrictedPlaces[j].place) {
                    //                                     userAllInfo.restrictedPlaces[j].deadline = new Date().getTime() + 5000;
                    //                                     addPlace = false;
                    //                                     break;
                    //                                 }
                    //                             }
                    //                             else if (userAllInfo.restrictedPlaces[j].deadline > new Date().getTime()) {
                    //                                 if (response.json.results[i].name === userAllInfo.restrictedPlaces[j].place) {
                    //                                     addPlace = false;
                    //                                     break;
                    //                                 }
                    //                             }
                    //                         }
                    //                     }

                    //                     if (addPlace) {
                    //                         console.log('Including place ' + response.json.results[i].name);
                    //                         foundPlacesGoogle[response.json.results[i].name] = {};
                    //                         foundPlacesGoogle[response.json.results[i].name]["name"] = response.json.results[i].name;
                    //                         foundPlacesGoogle[response.json.results[i].name]["location"] = response.json.results[i].geometry.location;
                    //                         foundPlacesGoogle[response.json.results[i].name]["id"] = response.json.results[i]["place_id"];
                    //                     }
                    //                     else {
                    //                         console.log('Excluding place ' + response.json.results[i].name);
                    //                     }
                    //                 }
                    //             })
                    //             .catch(function(err){
                    //                 console.log("Error from Google Places: ", err);
                    //                 return err;
                    //             });

                    //         searchPromiseArray.push(searchPromise);
                    //     }
                    // }
                }
                return data;
            })
            .catch(function(err){
                console.log("Query error: " + err.message);
                return err;
            });

        userInfoPromise
            .then(function() {
                //console.log("User interests: " + userInterests);
                //Get templates for user, given id and interests

                let options = {
                    method: 'POST',
                    url: 'https://limitless-ocean-14192.herokuapp.com/',
                    headers:{ 'content-type': 'application/json'},
                    body:
                        {
                            userId: 'test_user', //Change to actual user on new algorithm
                            data: [userInterests]
                        },
                    json: true
                };

                rp(options)
                    .then(function (body) {
                        console.log(body);
                        templates = body.prediction;
                        if (userId === 'pocotheo_ucl_ac_uk'){
                            templates.push('ACT-N');
                        }
                        console.log("Received templates: " + templates);
                    })
                    .catch(function (err) {
                        console.log("Error calling Python server: " + err.message);
                    });
            })
            .catch(function(err){
                console.log("Failed to set user preference and templates!", err);
            });

        Promise.all(searchPromiseArray)
            .then(function(){
                console.log("Google places: ", foundPlacesGoogle);

                //Match up results with metadata
                let placeMatches = [];
                let queryPromiseArray = [];

                let queryStart;
                queryStart = new Date();

                for (let place in foundPlacesGoogle) {
                    if (foundPlacesGoogle.hasOwnProperty(place)) {
                        queryParams["ExpressionAttributeValues"][':placeName'] = place;
                        let qPromise = dynamoDB.query(queryParams).promise()
                            .then(function(data){
                                if (data.Items && data.Items.length > 0) {
                                	//data.Items[0].placeId = foundPlacesGoogle.place.id;
                                    placeMatches.push(data.Items[0]);
                                    //console.log("Match " + placeMatches.length + data.Items[0]["Name"]);
                                }
                                return data;
                            })
                            .catch(function(err){
                                console.log("Query error: " + err.message);
                                return err;
                            });
                        queryPromiseArray.push(qPromise);
                    }
                }

                Promise.all(queryPromiseArray)
                    .then(function(){
                        let activity;
                        let locationResp;

                        if (!foundActivity) {
                            //Choose a template at random
                            activity = chooseTemplate(templates, placeMatches, foundPlacesGoogle, userPreferences, userTime);
                            locationResp = "lat:" + placeLocation.lat + ",lng:" + placeLocation.lng + ";";
                            placeLocation.alt = 0;
                        }

                        endTime = new Date();

                        let timeDiff = endTime - startTime; //in ms
                        // strip the ms
                        timeDiff /= 1000;

                        // get seconds
                        let seconds = Math.round(timeDiff);
                        console.log("Everything took " + seconds + " seconds");
                        console.log("Querying took " +  Math.round((endTime -  queryStart)/1000));

                        //Check users left strikes and reset if necessary
                        let params = {
                            TableName: "UserInfo",
                            Key: {
                                "UserID": userId
                            }
                        }

                        dynamoDB.get(params, function(err, data) {
                            let overLimit = false;

                            if (err) {
                                console.error("Unable to get data for user " + userId, err);
                
                                _response = buildOutput(500, err);
                                return callback(_response, null);
                            }
                            else{
                                console.log("Userinfo retrieved", data);
                                const now = new Date();
                                let currentStrikes = data.Item.strikes;
                                
                                if (now.getTime() >= data.Item.resetAt){
                                    data.Item.strikes = 1;
                                    
                                    const toMidnight = 24 - now.getHours()
                                    const deadline = toMidnight * 3600000 + now.getTime() - now.getMinutes() * 60000;

                                    data.Item.resetAt = deadline;
                                }
                                else if (currentStrikes <= 2){
                                	currentStrikes += 1;
                                }
                                else {
                                    overLimit = true;
                                }

                                let newActivity;

                                if (foundActivity) {
                                    newActivity = raincheckActivity;
                                }
                                else {
                                    //Update the user info with the activity which was just generated
                                    newActivity = {
                                        "activity": activity,
                                        "locations": [placeLocation],
                                        "id": placeId,
                                        "name": placeName + ", London (" + placeRating,
                                        "participants": [],
                                        "owner": userId,
                                        "template": activityTemplate
                                    };
                                }

                                if (activityDrink !== '') {
                                    newActivity.metadata = activityDrink;
                                }

                                data.Item.strikes = currentStrikes;
                                data.Item.activity = overLimit ? data.Item.activity:newActivity;
                                newActivity = data.Item.activity;
                                
                                if (data.Item.restrictedPlaces) {
                                    data.Item.restrictedPlaces = userAllInfo.restrictedPlaces;
                                }

                                if (data.Item.raincheck) {
                                    data.Item.raincheck = userAllInfo.raincheck;
                                }
                
                                params.Item = data.Item;
                
                                 // Call DynamoDB to add the item to the table
                                 dynamoDB.put(params, function(err, data) {
                                    if (err) {
                                        console.log("Error adding new activity", err);
                                        _response = buildOutput(500, err);
                                        return callback(_response, null);
                                    }
                                    else {
                                        console.log("Added new activity for " + userId);
                                        _response = buildOutput(200, {
                                            "activity": newActivity.activity,
                                            "locations": newActivity.locations,
                                            "id": newActivity.id,
                                            "name": newActivity.name,
                                            "overLimit": overLimit
                                        });
                                        return callback(null, _response);
                                    }
                                });
                            }
                        });
                    })
                    .catch(function(err){
                        console.log("Promise array error: " + err.message);
                        _response = buildOutput(500, err);
                        return callback(_response, null);
                    });
            })
            .catch(function(err){
                _response = buildOutput(500, err);
                return callback(_response, null);
            });
    }
    else {
        _response = buildOutput(500, invalid_path_err);
        return callback(_response, null);
    }
};


/* Utility functions */
function chooseTemplate(templates, placeMatches, allPlaces, userPreferences, userTime){
	let templateWeights = computeTemplateWeights(templates, userPreferences);

	console.log("Templates", templates);
	console.log("Weights", templateWeights);

    let rndPos = getRandomItem(templates, templateWeights);
    let templateInfo = localTemplates[templates[rndPos]];

    activityTemplate = templates[rndPos];
    console.log("Chose template: " + templates[rndPos]);

    if (templateInfo) {
        let chosenTemplate = "";

        if (templateInfo.hasOwnProperty("Template3")) {
            let rndNew = Math.random();
            if (rndNew < 0.33) {
                chosenTemplate = templateInfo["Template3"];
            }
        }
        if (chosenTemplate != "" && templateInfo.hasOwnProperty("Template1") && templateInfo.hasOwnProperty("Template2")) {
            chosenTemplate = Math.random() > 0.5 ? templateInfo["Template1"] : templateInfo["Template2"];
        }
        else {
            chosenTemplate = templateInfo["Template1"];
        }

        console.log("Random template nr = " + rndPos + ". Template: ", chosenTemplate);

        let nrOfPlaces = (chosenTemplate.match(/pub\/bar/g) || []).length;

        if (nrOfPlaces > 1){
            console.log("Multiple places template");
            chosenTemplate = fillInTemplateMultiplePlaces(templateInfo, chosenTemplate, clone(placeMatches), nrOfPlaces, userTime);
        }
        else {
            chosenTemplate = fillInTemplate(templateInfo, chosenTemplate, clone(placeMatches), userTime);
        }

        if (chosenTemplate != "") {
            console.log("Returning " + chosenTemplate);
            return chosenTemplate;
        }
        else {
            templates.splice(rndPos, 1);
            return chooseTemplate(templates, placeMatches, allPlaces, userPreferences, userTime);
        }
    }
    else{
        templates.splice(rndPos, 1);
        return chooseTemplate(templates, placeMatches, allPlaces, userPreferences, userTime);
    }
}

function fillInTemplateMultiplePlaces(info, template, allPlaces, nrOfPlaces, userTime){
    let chosenPlaces = choosePlaces(allPlaces, nrOfPlaces);
    let filledTemplate = "";
    let foundSpecialCharacter = 0;
    let indices = [];
    let templateOK = true;
    let placeWildcard;
    let lastIndexCopied = 0;

    for (let i = 0; i < template.length; i++) {
        if (template[i] === "*") {
            indices.push(i);
            foundSpecialCharacter++;
            if (foundSpecialCharacter % 2 == 0) {
                filledTemplate += template.substring(lastIndexCopied, indices[foundSpecialCharacter - 2]);
                lastIndexCopied = indices[foundSpecialCharacter - 1] + 1;
                let wildcard = template.substring(indices[foundSpecialCharacter - 2] + 1, indices[foundSpecialCharacter - 1]);

                if (wildcard.indexOf(',') != -1){
                    wildcard =  wildcard.substring(0, wildcard.length - 2);
                }

                console.log("Wild card " + wildcard);
                if (wildcard.indexOf("Var") != -1) {
                    let variable = chooseVar(clone(info[wildcard]), clone(chosenPlaces[0]), userTime);
                    if (variable != "") {
                        //Add "an", if variable starts with vowel and is proceeded by "a"
                        if ((variable[0].toLowerCase() == "a" || variable[0].toLowerCase() == "e" || variable[0].toLowerCase() == "i" || variable[0].toLowerCase() == "o" || variable[0].toLowerCase() == "u") && filledTemplate[filledTemplate.length - 2] == "a" && filledTemplate[filledTemplate.length - 3] == " ") {
                            filledTemplate = filledTemplate.substring(0, filledTemplate.length - 1) + "n ";
                        }
                        filledTemplate += variable;
                        let toReplace = '*' + wildcard + '*';
                        console.log(toReplace + " is replaced with " + variable);
                    }
                    else {
                        templateOK = false;
                        break;
                    }
                }
                else {
                    console.log("Place wildcard: " + wildcard);
                    filledTemplate += template.substring(indices[foundSpecialCharacter - 2], indices[foundSpecialCharacter - 1] + 1);
                    lastIndexCopied = indices[foundSpecialCharacter - 1] + 1;
                    placeWildcard = wildcard;
                }
                console.log("Filled so far: " + filledTemplate);
            }
        }
    }

    if (templateOK) {
        let toReplace = '*' + placeWildcard + '*';
        // console.log("Add place to: " + filledTemplate);

        for (let i = 0; i < nrOfPlaces; i++){
            filledTemplate = filledTemplate.replace(toReplace, chosenPlaces[i]);
        }

        filledTemplate = filledTemplate.charAt(0).toUpperCase() + filledTemplate.slice(1);

        placeName = chosenPlaces[0];
        placeLocation = foundPlacesGoogle[placeName]["location"];
        placePhoto = foundPlacesGoogle[placeName]["picture"];
        placeId = foundPlacesGoogle[placeName]["id"];
        placeRating = chosenPlace["rating"];

        return filledTemplate;
    }
    else {
        return "";
    }
}

function fillInTemplate(info, template, places, userTime){
    if (places.length > 0) {
        let chosenPlace = choosePlace(places);
        //console.log("Place is ", chosenPlace);

        //If there is a special activity give it with a probability of 30%
        if (chosenPlace.hasOwnProperty("Specials") && Math.random() < 0.3) {
            placeName = chosenPlace["Name"];
            placeLocation = foundPlacesGoogle[chosenPlace["Name"]]["location"];
            placePhoto = foundPlacesGoogle[chosenPlace["Name"]]["picture"];
            placeId = foundPlacesGoogle[chosenPlace["Name"]]["id"];
            placeRating = chosenPlace["rating"];
            return chosenPlace["Specials"][Math.floor(Math.random() * chosenPlace["Specials"].length)];
        }
        //Fill in the template
        else {
            let filledTemplate = "";
            let foundSpecialCharacter = 0;
            let indices = [];
            let templateOK = true;
            let placeWildcard;
            let lastIndexCopied = 0;

            for (let i = 0; i < template.length; i++) {
                if (template[i] === "*") {
                    indices.push(i);
                    foundSpecialCharacter++;
                    if (foundSpecialCharacter % 2 == 0) {
                        filledTemplate += template.substring(lastIndexCopied, indices[foundSpecialCharacter - 2]);
                        lastIndexCopied = indices[foundSpecialCharacter - 1] + 1;
                        let wildcard = template.substring(indices[foundSpecialCharacter - 2] + 1, indices[foundSpecialCharacter - 1]);
                        console.log("Wild card " + wildcard);
                        if (wildcard.indexOf("Var") != -1) {
                            let variable = chooseVar(clone(info[wildcard]), clone(chosenPlace), userTime);
                            if (variable != "") {
                                //Add "an", if variable starts with vowel and is proceeded by "a"
                                if ((variable[0].toLowerCase() == "a" || variable[0].toLowerCase() == "e" || variable[0].toLowerCase() == "i" || variable[0].toLowerCase() == "o" || variable[0].toLowerCase() == "u") && filledTemplate[filledTemplate.length - 2] == "a" && filledTemplate[filledTemplate.length - 3] == " ") {
                                    filledTemplate = filledTemplate.substring(0, filledTemplate.length - 1) + "n ";
                                }
                                filledTemplate += variable;
                                let toReplace = '*' + wildcard + '*';
                                console.log(toReplace + " is replaced with " + variable);
                            }
                            else {
				                console.log("Did not find a good Var");
                                templateOK = false;
                                break;
                            }
                        }
                        else {
                            console.log("Place wildcard: " + wildcard);
                            filledTemplate += template.substring(indices[foundSpecialCharacter - 2], indices[foundSpecialCharacter - 1] + 1);
                            lastIndexCopied = indices[foundSpecialCharacter - 1] + 1;
                            placeWildcard = wildcard;
                        }
                    }
                }
            }

            if (templateOK) {
                let toReplace = '*' + placeWildcard + '*';
                console.log("Add place to: " + filledTemplate);
                filledTemplate += template.substring(lastIndexCopied);
                console.log("Completed Template: " + filledTemplate);
                filledTemplate = filledTemplate.replace(toReplace, chosenPlace["Name"]);
                filledTemplate = filledTemplate.charAt(0).toUpperCase() + filledTemplate.slice(1);

                placeName = chosenPlace["Name"];
                placeLocation = foundPlacesGoogle[chosenPlace["Name"]]["location"];
                placePhoto = foundPlacesGoogle[chosenPlace["Name"]]["picture"];
                placeId = foundPlacesGoogle[chosenPlace["Name"]]["id"];
                placeRating = chosenPlace["rating"];

                return filledTemplate;
            }
            else {
                places.splice(places.indexOf(chosenPlace), 1);
                return fillInTemplate(info, template, places);
            }
        }
    }
    else{
        return "";
    }
}

function choosePlaces(places, nrOfPlaces){
    let searchThrough = places instanceof Array ? places : Object.keys(places);

    let rndPos = Math.floor(Math.random() * searchThrough.length);
    let chosenIndices = [];
    let chosenPlaces = [];
    chosenIndices.push(rndPos);
    chosenPlaces.push(searchThrough[rndPos]);
    let choseSoFar = 1;

    while (choseSoFar < nrOfPlaces) {
        rndPos = Math.floor(Math.random() * searchThrough.length);
        if (!chosenIndices.includes(rndPos)) {
            chosenPlaces.push(searchThrough[rndPos]);
            choseSoFar++;
        }
    }

    return chosenPlaces;
}

function choosePlace(places){
    console.log("Choosing a place from", places);

    let rndPos = Math.floor(Math.random() * places.length);
    return places[rndPos];

    const days = ['Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday'];
    const day = days[new Date().getDay()];
    const hour = new Date().getHours();
    let minSum = 100000;
    let bestPlaceIndex = -1;

    for (let i = 0; i < places.length; i++) {
        let currentSum = 0;
        let skipPlace = true;

        if (!(places[i].populartimes && places[i].populartimes[day][hour] === 0)) {
            if (userAllInfo.distanceRestriction) {
                const distToPlace = distance(userLat, userLng, place[i].coordinates.lat, place[i].coordinates.lng);
                if (distToPlace <= userAllInfo.distanceRestriction) {
                    currentSum += distToPlace;
                    skipPlace = false;
                }
            }

            if (!skipPlace) {
                if (places[i].populartimes) {
                    currentSum += places[i].populartimes[day][hour];
                }
                else {
                    currentSum += 50;
                }

                if (currentSum < minSum) {
                    minSum = currentSum;
                    bestPlaceIndex = i;
                }
            }
        }
    }

    //TODO deal with everything is closed or everything is too far away
    if (bestPlaceIndex > -1 && minSum < 100000) {
        return places[bestPlaceIndex];
    }
    else {
        let rndPos = Math.floor(Math.random() * places.length);
        return places[rndPos];
    }
}

function chooseVar(choises, place, userTime){
    console.log("var fill in choises: ", choises);
    let saveDrinkVar = false;
    if (choises.length > 0){
        let restrictedData = typeof userAllInfo.restrictedMetadata !== 'undefined' ? userAllInfo.restrictedMetadata : [];
        let filteredInfo = choises.diff(restrictedData);
        console.log('Filtered choises: ', filteredInfo);
        let rndPos = Math.floor(Math.random() * filteredInfo.length);
        let chosen = "";
        
        if (choises.includes("metadata:Cocktail")){
            chosen = "metadata:Cocktail";
            rndPos = choises.indexOf("metadata:Cocktail");
        }
        else if (choises.includes("metadata:Whiskey")){
            chosen = "metadata:Whiskey";
            rndPos = choises.indexOf("metadata:Whiskey");
        }
        else if (choises.includes("metadata:Wine")){
            chosen = "metadata:Wine";
            rndPos = choises.indexOf("metadata:Wine");
        }
        else if (choises.includes("metadata:Shot")){
            chosen = "metadata:Shot";
            rndPos = choises.indexOf("metadata:Shot");
        }
        else if (choises.includes("metadata:Beer")){
            chosen = "metadata:Beer";
            rndPos = choises.indexOf("metadata:Beer");
        }
        else if (choises.includes("metadata:Gin")){
            chosen = "metadata:Gin";
            rndPos = choises.indexOf("metadata:Gin");
        }
        else if (choises.includes("metadata:Rum")){
            chosen = "metadata:Rum";
            rndPos = choises.indexOf("metadata:Rum");
        }
        else if (choises.includes("metadata:Cognac")){
            chosen = "metadata:Cognac";
            rndPos = choises.indexOf("metadata:Cognac");
        }        
        else if (choises.includes("metadata:Drinks")){
            chosen = "metadata:Drinks";
            rndPos = choises.indexOf("metadata:Drinks");
        }
        else{
            chosen = filteredInfo[rndPos];
        }

	    console.log("Chosen is " + chosen + " of type " + typeof chosen);
	    let timeConstraint = -1;

        //If the variable is an object then it has a time constraint on it
        if (typeof chosen != "string") {
			timeConstraint = chosen[Object.keys(chosen)[0]];
			chosen = Object.keys(chosen)[0];
			console.log("User time " + userTime + "TimeConstranint " + timeConstraint + ". Chosen " + chosen);
		}

		//if (timeConstraint <= userTime){
			if (chosen.indexOf("metadata") != -1){
				//console.log(chosen);
				let info = chosen.split(':')[1];
                if (info === 'Cocktail' || info === 'Drinks' || info === 'Juice' || info === 'Tea' || info === 'Coffee' || info === 'Beer' || info === 'Shots' || info === 'Wine' || info === 'Gin' || info === 'Rum' || info === 'Whiskey' || info === 'Cognac') {
                    saveDrinkVar = true;
                }
				//console.log("Metadata category " + info + " options: ", place[info]);
				if (place.hasOwnProperty(info)){
					if (!(place[info] instanceof Array)){
						place[info] = place[info].split(',');
					}
					//console.log("Metadata category " + info + " options (should be array): ", place[info]);
                    filteredInfo = place[info].diff(restrictedData);
                    console.log('Filtered choises: ', filteredInfo);
                    let chosenVar = filteredInfo[Math.floor(Math.random() * filteredInfo.length)];
                    if (saveDrinkVar) {
                        activityDrink = chosenVar;
                    }
					return chosenVar;
				}
				else{
					choises.splice(rndPos, 1);
					return chooseVar(choises, place, userTime);
				}
			}
			else{
                if (chosen.indexOf('beer') !== -1 || chosen.indexOf('ale') !== -1 || chosen.indexOf('shot') !== -1) {
                    activityDrink = chosen;
                }
				return chosen;
			}
		/*}
		else{
			choises.splice(rndPos, 1);
			return chooseVar(choises, place, userTime);
		}*/
    }
    else{
        return "";
    }
}

function computeTemplateWeights (templates, userPreferences){
	let weights = [];

	for (let i = 0; i < templates.length; i++){
		weights.push(userPreferences[templates[i]]);
	}

	return weights;
}

function rand(min, max) {
	return Math.random() * (max - min) + min;
}

function getRandomItem(list, weights) {
	let total_weight = weights.reduce(function (prev, cur, i, arr) {
		return prev + cur;
	});

	let random_num = rand(0, total_weight);
	let weight_sum = 0;
	//console.log(random_num)

	for (let i = 0; i < list.length; i++) {
		weight_sum += weights[i];
		weight_sum = +weight_sum.toFixed(2);

		if (random_num <= weight_sum) {
			return i;
		}
	}
}

function getPlaces(query, pageToken){
    let resultToken;
    if (pageToken){
        query.pagetoken = pageToken;
    }

    return PlacesClient.placesNearby(query).asPromise()
        .then(function(response) {
            for (let i = 0; i < response.json.results.length; i++) {
                places.push(response.json.results[i].name);
            }

            resultToken = response.json.next_page_token;
            return sleep(2000)
                .then(function(){
                    return resultToken;
                });
        })
        .catch(function(err){
            console.log("Error from Google Places: " + err.message);
            return err;
        });
}

function distance(lat1, lon1, lat2, lon2) {
    if ((lat1 == lat2) && (lon1 == lon2)) {
        return 0;
    }
    else {
        let radlat1 = Math.PI * lat1/180;
        let radlat2 = Math.PI * lat2/180;
        let theta = lon1-lon2;
        let radtheta = Math.PI * theta/180;
        let dist = Math.sin(radlat1) * Math.sin(radlat2) + Math.cos(radlat1) * Math.cos(radlat2) * Math.cos(radtheta);
        if (dist > 1) {
            dist = 1;
        }
        dist = Math.acos(dist);
        dist = dist * 180/Math.PI;
        dist = dist * 60 * 1.1515;
        return dist * 1.609344;
    }
}

function clone(obj){
    if(obj == null || typeof(obj) != 'object')
        return obj;

    var temp = new obj.constructor(); 
    for(var key in obj)
        temp[key] = clone(obj[key]);

    return temp;
}

Array.prototype.diff = function(a) {
    return this.filter(function(i) {return a.indexOf(i) < 0;});
};

// Build HTTP response for the microservices output
function buildOutput(statusCode, data) {
    let _response = {
        "statusCode": statusCode,
        "headers": {
            "Access-Control-Allow-Origin": "*",
            'content-type': 'application/json'
        },
        "body": JSON.stringify(data),
		"isBase64Encoded": false
    };
    return _response;
}
