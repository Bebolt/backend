/**
 * Created by George on 11/06/2018.
 */

const lambdaLocal = require('lambda-local');
const path = require("path");

var jsonPayload = {
    "resource": "/login",
    "httpMethod": "POST",
    "pathParams": "{}",
    "queryStringParameters": {
        "userId": "stoian.george95@gmail.com",
        "userPass": 'Parolamea95'
    },
    "payload": {}
};

//userId="test_user"&lat=51.518170&lng=-0.143651&type="Normal"&mood="Booze"
lambdaLocal.execute({
    event: jsonPayload,
    lambdaPath: path.join(__dirname, './handler.js'),
    profilePath: '~/.aws/credentials',
    profileName: 'default',
    timeoutMs: 100000
}).then(function(done) {
    console.log(done);
}).catch(function(err) {
    console.log(err);
});
//
/*
var jsonPayloadPref = {
    "resource": "/",
    "httpMethod": "POST",
    "pathParams": "{}",
    "queryStringParameters": {
        "userId": "test_user",
        "templateId": 'ACT-1',
        "score": -0.5
    },
    "payload": {}
};

//userId="test_user"&templateId="ACT-10"&score=13
lambdaLocal.execute({
    event: jsonPayloadPref,
    lambdaPath: path.join(__dirname, '../PreferencesMicroservice/index.js'),
    profilePath: '~/.aws/credentials',
    profileName: 'default',
    timeoutMs: 100000
}).then(function(done) {
    console.log(done);
}).catch(function(err) {
    console.log(err);
});
*/