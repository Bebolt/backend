/**
 * Created by George on 18/03/2017.
 */

'use strict';

// dependencies
global.fetch = require('node-fetch');
let AmazonCognitoIdentity = require('amazon-cognito-identity-js');
let CognitoUserPool = AmazonCognitoIdentity.CognitoUserPool;

let P = require('bluebird');
let uuid = require('uuid/v4');

// AWS
var AWS = require('aws-sdk');
AWS.config.update({region: 'eu-west-2'});
AWS.config.setPromisesDependency(P);

const poolData = {
    UserPoolId : 'eu-west-2_z4bxpN2zw', // Your user pool id here
    ClientId : '3oat6b09h4cl1sek13jk39ao2i' // Your client id here
};
const userPool = new AmazonCognitoIdentity.CognitoUserPool(poolData);

var dynamoDB = new AWS.DynamoDB.DocumentClient({apiVersion: '2012-08-10'});
var sns = new AWS.SNS({apiVersion: '2010-03-31'});

//Helper for array difference
Array.prototype.diff = function(a) {
    return this.filter(function(i) {return a.indexOf(i) < 0;});
};

/**
 * Provide an event that contains the following keys:
 *
 * - resource: API Gateway resource for event
 * - path: path of the HTTPS request to the microservices API call
 * - httpMethod: HTTP method of the HTTPS request from microservices API call
 * - headers: HTTP headers for the HTTPS request from microservices API call
 * - queryStringParameters: query parameters of the HTTPS request from microservices API call
 * - pathParameters: path parameters of the HTTPS request from microservices API call
 * - stageVariables: API Gateway stage variables, if applicable
 * - body: body of the HTTPS request from the microservices API call
 */
module.exports.handler = function (event, context, callback) {
    console.log(event);
    let _response = "";
    let invalid_path_err = {"Error": "Invalid path request " + event.resource + ', ' + event.httpMethod};

    //Deal with incoming request for an activity
    if (event.resource === '/login' && event.httpMethod === "POST") {
        console.log("Logging user in");
        let userId = event.queryStringParameters.userId;
        let userPass = event.queryStringParameters.userPass;

        var authenticationData = {
            Username : userId,
            Password : userPass,
        };
        var authenticationDetails = new AmazonCognitoIdentity.AuthenticationDetails(authenticationData);
        var userData = {
            Username : userId,
            Pool : userPool
        };
        var cognitoUser = new AmazonCognitoIdentity.CognitoUser(userData);
        cognitoUser.authenticateUser(authenticationDetails, {
            onSuccess: function (result) {
				console.log("USER: ", result.getAccessToken().payload);
                var accessToken = result.getAccessToken().getJwtToken();
                
                let params = {
                    TableName: "UserInfo",
                    Key: {
                        "UserID": result.getAccessToken().payload.username
                    }
                };
        
                dynamoDB.get(params, function(err, data) {
                    if (err) {
                        console.error("Unable to get data for user " + result.getAccessToken().payload.username, err);
        
                        _response = buildOutput(500, err);
                        return callback(null, _response);
                    }
                    else{
                        console.log("Userinfo retrieved", data);

                        _response = buildOutput(200, {
                            "token": accessToken,
                            "username": data.Item.name,
                            "activity": data.Item.activity
                        });
                        return callback(null, _response);
                    }
                });
            },
     
            onFailure: function(err) {
                console.log(err.message || JSON.stringify(err));
                
                _response = buildOutput(500, err);
                return callback(null, _response);
            },
        });
    }
    else if (event.resource === '/password/change' && event.httpMethod === "POST") {
        let userId = event.queryStringParameters.userId;
        let userPass = event.queryStringParameters.userPass;
        let userNewPass = event.queryStringParameters.userNewPass;

        const authenticationData = {
            Username : userId,
            Password : userPass,
        };
        const authenticationDetails = new AmazonCognitoIdentity.AuthenticationDetails(authenticationData);
    

        let userData = {
            Username : userId,
            Pool : userPool
        };
        let cognitoUser = new AmazonCognitoIdentity.CognitoUser(userData);
        cognitoUser.authenticateUser(authenticationDetails, {
            onSuccess: function (result) {
                cognitoUser.changePassword(userPass, userNewPass, function(err, result) {
                    if (err) {
                        console.log("Unable to change user's password: " + userId, err);
                
                        _response = buildOutput(500, err);
                        return callback(null, _response);
                    }
                    else {
                        console.log("Successfully changed user's password");
                        _response = buildOutput(200, {
                            "status": "OK"
                        });
                        return callback(null, _response);
                    }
                });
            },

            onFailure: function(err) {
                console.log("Unable to log in user before changing password: " + userId, err);
                
                _response = buildOutput(500, err);
                return callback(null, _response);
            },

        });
    }
    else  if (event.resource === '/activity/invite' && event.httpMethod === "POST") {
        //userId=stoian_george_ucl_ac_uk&userName=George Stoian&friendId=test_user&activity=Don't have a cocktail, have an Artisan Moment at Artesian&
        //activityLocation=51.5175891,-0.1438754,0&activityImage=ChIJEx8Vq9UadkgRaPTTkQzdmmk&activityAddress=Artesian, London
        let userId = event.queryStringParameters.userId;
        let userName = event.queryStringParameters.userName;
        let friendId = event.queryStringParameters.friendId;
        let activity = event.queryStringParameters.activity;
        let activityAddress = event.queryStringParameters.activityAddress;
        let activityImage = event.queryStringParameters.activityImage;
        let activityLocation = event.queryStringParameters.activityLocation;

        let activityLat = activityLocation.split(',')[0];
        let activityLng = activityLocation.split(',')[1];
        let activityAlt = activityLocation.split(',')[2];

        let friendIds = friendId.split(',');
        console.log('To invite', friendIds);

        let params = {
            TableName: "UserInfo",
            Key: {
                "UserID": userId
            }
        };

        dynamoDB.get(params, function(err, data) {
            if (err) {
                console.error("Unable to get data for user " + userId, err);

                _response = buildOutput(500, err);
                return callback(null, _response);
            }
            else{
                console.log("Userinfo retrieved", data);

                let updateActivity = {
                    "activity": activity,
                    "locations": [{
                        "lat": activityLat,
                        "lng": activityLng,
                        "alt": activityAlt
                    }],
                    "id": activityImage,
                    "name": activityAddress,
                    "owner": data.Item.activity.owner,
                    "participants": data.Item.activity.participants
                };
                let notification = {
                    "name": userName + " invited you to participate to: " + activity,
                    "time": new Date().getTime(),
                    "status": "unread",
                    "read": false,
                    "id": uuid(),
                    "from": {
                        id: userId,
                        name: data.Item.name
                    },
                    "activity": updateActivity
                };

                if (typeof data.Item.activityInvited === 'undefined'){
                    data.Item.activityInvited = friendIds;
                }
                else {
                    data.Item.activityInvited.concat(friendIds);
                }

                console.log('User invited', data.Item.activityInvited);

                params.Item = data.Item;

                dynamoDB.put(params, function(err, data) {
                    if (err) {
                        console.log("Error noting friend as invited to activity", err);
                        _response = buildOutput(500, err);
                        return callback(null, _response);
                    } else {
                        console.log("Successfully noted friend as invited to activity", data);

                        friendIds.forEach(function(friend) {
                            params = {
                                TableName: "UserInfo",
                                Key: {
                                    "UserID": friend
                                },
                                UpdateExpression: 'set #notifications = list_append(#notifications, :newNotification)',
                                ExpressionAttributeNames: {
                                    '#notifications': 'notifications'
                                },
                                ExpressionAttributeValues: {
                                    ':newNotification': [notification]
                                }
                            };

                            dynamoDB.update(params).promise();
                        });

                        //SNS does not support endpoint creation in eu-west-2
                        AWS.config.update({region: 'eu-west-1'});
                        sns = new AWS.SNS({apiVersion: '2010-03-31'});

                        let paramsSNS = {
                            TargetArn: data.Item.endpointARN,
                            MessageStructure: 'json', // so we can put in a custom payload and message
                            Message: JSON.stringify({
                                APNS: JSON.stringify({
                                    aps: {
                                        alert: {
                                            title : "Activity invitation",
                                            body : `${userName} invited you to participate to an activity`,
                                            sound: "default",
                                            badge: 1
                                        }
                                    },
                                    //You can add your custom data here.
                                    type : "notification",
                                    "notification-id": notification.id
                                }),
                            }),
                        };

                        sns.publish(paramsSNS, function(err, res) {
                            if (err) {
                                console.log(err, err.stack); // an error occurred
                            }
                            else {
                                console.log("Sent push notification successfully", res);
                            }

                            AWS.config.update({region: 'eu-west-2'});

                            _response = buildOutput(200, {"status": 'OK'});
                            return callback(null, _response);
                        });
                    }
                });
            }
        });
    }
    else if (event.resource === '/activity/cancel' && event.httpMethod === "POST") {
        let userId = event.queryStringParameters.userId;
        let notificationId = event.queryStringParameters.notificationId;

        let params = {
            TableName: "UserInfo",
            Key: {
                "UserID": userId
            }
        };

        dynamoDB.get(params, function(err, data) {
            if (err) {
                console.error("Unable to get data for user " + userId, err);

                _response = buildOutput(500, err);
                return callback(null, _response);
            }
            else {
                console.log("Userinfo retrieved", data);
                let fromUser;
                let activityName;
                const userName = data.Item.name;

                //Update notification status
                data.Item.notifications.forEach(function (notification) {
                    if (notification.id === notificationId){
                        notification.status = 'canceled';
                        notification.read = true;
                        activityName = notification.activity.activity;
                        fromUser = notification.from.id;
                    }
                });

                params.Item = data.Item;

                 // Call DynamoDB to add the item to the table
                 dynamoDB.put(params, function(err, data) {
                    if (err) {
                        console.log("Error cancelling user activity invite", err);
                        _response = buildOutput(500, err);
                        return callback(null, _response);
                    } else {
                        console.log("Successfully canceled activity invite", data);

                        let notification = {
                            name: userName + " canceled your invite to participate to: " + activityName,
                            time: new Date().getTime(),
                            status: "canceled",
                            read: false,
                            id: uuid(),
                            from: {
                                id: userId,
                                name: userName
                            }
                        };

                        params = {
                            TableName: "UserInfo",
                            Key: {
                                "UserID": fromUser
                            }
                        };
                        
                        dynamoDB.get(params, function(err, data) {
                            if (err) {
                                console.error("Unable to get data for user " + fromUser, err);

                                _response = buildOutput(500, err);
                                return callback(null, _response);
                            }
                            else {
                                console.log("Userinfo retrieved", data);

                                data.Item.notifications.push(notification);
                                const invited = data.Item.activityInvited;

                                for (let i = 0; i < data.Item.activityInvited.length; i++) {
                                    if (userId === data.Item.activityInvited[i].id){
                                        invited.splice(i,1);
                                        break;
                                    }
                                }

                                data.Item.activityInvited = invited;

                                params.Item = data.Item;

                                dynamoDB.put(params, function(err, data) {
                                    if (err) {
                                        console.log("Error adding new notification", err);
                                        _response = buildOutput(500, err);
                                        return callback(null, _response);
                                    }
                                    else {
                                        console.log("Added new notification");

                                        //SNS does not support endpoint creation in eu-west-2
                                        AWS.config.update({region: 'eu-west-1'});
                                        sns = new AWS.SNS({apiVersion: '2010-03-31'});

                                        let paramsSNS = {
                                            TargetArn: data.Item.endpointARN,
                                            MessageStructure: 'json', // so we can put in a custom payload and message
                                            Message: JSON.stringify({
                                                APNS: JSON.stringify({
                                                    aps: {
                                                        alert: {
                                                            title : "Activity invitation",
                                                            body : `${userName} rejected your activity invitation`,
                                                            sound: "default",
                                                            badge: 1
                                                        }
                                                    },
                                                    //You can add your custom data here.
                                                    type : "notification",
                                                    "notification-id": notification.id
                                                }),
                                            }),
                                        };

                                        sns.publish(paramsSNS, function(err, res) {
                                            if (err) {
                                                console.log(err, err.stack); // an error occurred
                                            }
                                            else {
                                                console.log("Sent push notification successfully", res);
                                            }

                                            AWS.config.update({region: 'eu-west-2'});

                                            _response = buildOutput(200, {"status": 'OK'});
                                            return callback(null, _response);
                                        });
                                    }
                                });
                            }
                        });
                    }
                });
            }
        });
    }
    else if (event.resource === '/activity/join' && event.httpMethod === "POST") {
        let userId = event.queryStringParameters.userId;
        let notificationId = event.queryStringParameters.notificationId;

        let params = {
            TableName: "UserInfo",
            Key: {
                "UserID": userId
            }
        };

        dynamoDB.get(params, function(err, data) {
            if (err) {
                console.error("Unable to get data for user " + userId, err);

                _response = buildOutput(500, err);
                return callback(null, _response);
            }
            else{
                console.log("Userinfo retrieved", data);

                const newParticipant = {
                    "id": userId,
                    "name": data.Item.name
                };
                let fromUser = "";
                const userName = data.Item.name;

                //Update notification status
                data.Item.notifications.forEach(function (notification) {
                    if (notification.id === notificationId){
                        notification.status = 'joined';
                        notificatoin.read = true;
                        fromUser = notification.from.id;
                        data.Item.activity = notification.activity;
                    }
                });

                params.Item = data.Item;

                 // Call DynamoDB to add the item to the table
                 dynamoDB.put(params, function(err, data) {
                    if (err) {
                        console.log("Error joining user activity invite", err);
                        _response = buildOutput(500, err);
                        return callback(null, _response);
                    } else {
                        console.log("Successfully joined activity invite", data);

                        params = {
                            TableName: "UserInfo",
                            Key: {
                                "UserID": fromUser
                            }
                        };

                        dynamoDB.get(params, function(err, data) {
                            if (err) {
                                console.log("Error getting user information for user " + fromUser, err);
                                _response = buildOutput(500, err);
                                return callback(null, _response);
                            } else {
                                let notification = {
                                    name: newParticipant.name + " accepted your invite to participate to: " + data.Item.activity.activity,
                                    time: new Date().getTime(),
                                    status: "joined",
                                    read: false,
                                    id: uuid(),
                                    from: {
                                        id: userId,
                                        name: userName
                                    }
                                };
                                data.Item.notifications.push(notification);
                                let isOwner = fromUser === data.Item.activity.owner;
                                const ownerId = data.Item.activity.owner;

                                if (isOwner){
                                    data.Item.activity.participants.push(newParticipant);
                                }

                                params.Item = data.Item;

                                dynamoDB.put(params, function(err, data) {
                                    if (err) {
                                        console.log("Error updating user with new activity participant", err);
                                        _response = buildOutput(500, err);
                                        return callback(null, _response);
                                    } else {
                                        if (isOwner) {
                                            _response = buildOutput(200, {"status": "OK"});
                                            return callback(null, _response);
                                        } else {
                                            params = {
                                                TableName: "UserInfo",
                                                Key: {
                                                    "UserID": ownerId
                                                }
                                            };

                                            dynamoDB.get(params, function(err, data) {
                                                if (err) {
                                                    console.log("Error getting user information for user " + ownerId, err);
                                                    _response = buildOutput(500, err);
                                                    return callback(null, _response);
                                                } else {
                                                    data.Item.activity.participants.push(newParticipant);

                                                    params.Item = data.Item;

                                                    dynamoDB.put(params, function(err, data) {
                                                        if (err) {
                                                            console.log("Error updating user with new activity participant", err);
                                                            _response = buildOutput(500, err);
                                                            return callback(null, _response);
                                                        } else {
                                                            //SNS does not support endpoint creation in eu-west-2
                                                            AWS.config.update({region: 'eu-west-1'});
                                                            sns = new AWS.SNS({apiVersion: '2010-03-31'});

                                                            let paramsSNS = {
                                                                TargetArn: data.Item.endpointARN,
                                                                MessageStructure: 'json', // so we can put in a custom payload and message
                                                                Message: JSON.stringify({
                                                                    APNS: JSON.stringify({
                                                                        aps: {
                                                                            alert: {
                                                                                title : "Activity invitation",
                                                                                body : `${userName} accepted your activity invitation`,
                                                                                sound: "default",
                                                                                badge: 1
                                                                            }
                                                                        },
                                                                        //You can add your custom data here.
                                                                        type : "notification",
                                                                        "notification-id": notification.id
                                                                    }),
                                                                }),
                                                            };

                                                            sns.publish(paramsSNS, function(err, res) {
                                                                if (err) {
                                                                    console.log(err, err.stack); // an error occurred
                                                                }
                                                                else {
                                                                    console.log("Sent push notification successfully", res);
                                                                }

                                                                AWS.config.update({region: 'eu-west-2'});

                                                                _response = buildOutput(200, {"status": 'OK'});
                                                                return callback(null, _response);
                                                            });
                                                        }
                                                    });
                                                }
                                            });
                                        }
                                    }
                                });
                            }
                        });
                    }
                });
            }
        });
    }
    else if (event.resource === '/activity/left' && event.httpMethod === "GET") {
        let userId = event.queryStringParameters.userId;

        let params = {
            TableName: "UserInfo",
            Key: {
                "UserID": userId
            }
        };

        dynamoDB.get(params, function(err, data) {
            if (err) {
                console.error("Unable to get data for user " + userId, err);

                _response = buildOutput(500, err);
                return callback(_response, null);
            }
            else{
                console.log("Userinfo retrieved", data);
                _response = buildOutput(200, {
                    "activitiesLeft": 3 - data.Item.strikes
                });
                return callback(null, _response);
            }
        });
    }
    else if (event.resource === '/activity/friendstoinvite' && event.httpMethod === "GET") {
        let userId = event.queryStringParameters.userId;
 
        let params = {
            TableName: "UserInfo",
            Key: {
                "UserID": userId
            }
        };
 
        dynamoDB.get(params, function(err, data) {
            if (err) {
                console.error("Unable to get data for user " + userId, err);
 
                _response = buildOutput(500, err);
                return callback(_response, null);
            }
            else{
                console.log("Userinfo retrieved", data);
                
                let participantsIds = [];
                data.Item.activity.participants.forEach(function(user) {
                    participantsIds.push(user.id);
                });
                
                const invited = participantsIds.concat(data.Item.activityInvited);
                
                let friendsIds = [];
                data.Item.friends.forEach(function(friend) {
                    friendsIds.push(friend.id);
                });

                const toInviteId = friendsIds.diff(invited);
                let toInvite = [];
 
                toInviteId.forEach(function(invitee) {
                    data.Item.friends.forEach(function(friend) {
                        if (invitee == friend.id){
                            toInvite.push(friend);
                        }
                    });
                });
 
                _response = buildOutput(200, {
                    "users": toInvite
                });
                return callback(null, _response);
            }
        });
    }
    else if (event.resource === '/activity' && event.httpMethod === "GET") {
        let userId = event.queryStringParameters.userId;

        let params = {
            TableName: "UserInfo",
            Key: {
                "UserID": userId
            }
        };

        dynamoDB.get(params, function(err, data) {
            if (err) {
                console.error("Unable to get data for user " + userId, err);

                _response = buildOutput(500, err);
                return callback(_response, null);
            }
            else{
                console.log("Userinfo retrieved", data);
                _response = buildOutput(200, {
                    "activity": data.Item.activity
                });
                return callback(null, _response);
            }
        });
    }
    else if (event.resource === '/activity' && event.httpMethod === "DELETE") {
        let userId = event.queryStringParameters.userId;
        let reason = Number.parseInt(event.queryStringParameters.reason);
        let distance = Number.parseFloat(event.queryStringParameters.distance);

        let params = {
            TableName: "UserInfo",
            Key: {
                "UserID": userId
            }
        };

        dynamoDB.get(params, function(err, data) {
            if (err) {
                console.error("Unable to get data for user " + userId, err);

                _response = buildOutput(500, err);
                return callback(_response, null);
            }
            else{
                console.log("Userinfo retrieved", data);

                if (reason === 1) {
                    if (!data.Item.restrictedPlaces) {
                        data.Item.restrictedPlaces = [];
                    }

                    data.Item.restrictedPlaces.push({
                        'place': data.Item.activity.name.substring(0, data.Item.activity.name.lastIndexOf(",")),
                        'deadline': new Date().getTime() + 2592000000
                    });
                }
                else if (reason === 2) {
                    if (!data.Item.restrictedMetadata) {
                        data.Item.restrictedMetadata = [];
                    }
                    data.Item.restrictedMetadata.push(data.Item.activity.metadata);
                }
                else if (reason === 3) {
                    if (!data.Item.raincheck) {
                        data.Item.raincheck = [];
                    }
                    data.Item.raincheck.push({
                        'activity': data.Item.activity,
                        'deadline': new Date().getTime() + 604800000
                    });

                    if (!data.Item.restrictedPlaces) {
                        data.Item.restrictedPlaces = [];
                    }

                    data.Item.restrictedPlaces.push({
                        'place': data.Item.activity.name.substring(0, data.Item.activity.name.lastIndexOf(",")),
                        'deadline': 'once'
                    });
                }
                //TODO at first reappearence increase deadline to 14 days
                else if (reason === 4) {
                    if (data.Item.Preferences[data.Item.activity.template] > 0) {
                        data.Item.Preferences[data.Item.activity.template] -= 0.2;
                    }

                    if (!data.Item.restrictedTemplates) {
                        data.Item.restrictedTemplates = [];
                    }

                    data.Item.restrictedTemplates.push({
                        'template': data.Item.activity.template,
                        'deadline': new Date().getTime() + 259200000
                    })
                }
                else if (reason === 5) {
                    if (!data.Item.raincheck) {
                        data.Item.raincheck = [];
                    }
                    data.Item.raincheck.push({
                        'activity': data.Item.activity,
                        'deadline': new Date().getTime() + 21600000
                    });
                }
                else if (reason === 6) {
                    if (!data.Item.distanceRestriction) {
                        data.Item.distanceRestriction = 100000;
                    }
                    if (data.Item.distanceRestriction === 100000) {
                        if (!data.Item.refusedForDistance) {
                            const resetAt = new Date().getTime() + 7200000;
                            data.Item.refusedForDistance = {
                                'flag': true,
                                'reset': resetAt
                            }
                        }
                        else {
                            if (data.Item.refusedForDistance.flag) {
                                data.Item.refusedForDistance = {
                                    'flag': false,
                                }
                                data.Item.distanceRestriction = distance;
                            }
                        }
                    }
                    else {
                        data.Item.distanceRestriction = distance;
                    }
                }

                data.Item.activity = {};
                data.Item.activityInvited = [];

                params.Item = data.Item;

                dynamoDB.put(params, function(err, data) {
                    if (err) {
                        console.log("Error deleting refused activity", err);
                        _response = buildOutput(500, err);
                        return callback(_response, null);
                    }
                    else {
                        console.log("Deleted old activity for user " + userId);
                        _response = buildOutput(200, {
                            "status": "OK"
                        });
                        return callback(null, _response);
                    }
                });
            }
        });
    }
    else if (event.resource === '/user' && event.httpMethod === "GET") {
        let userId = event.queryStringParameters.userId;

        let params = {
            TableName: "UserInfo",
            Key: {
                "UserID": userId
            }
        };

        dynamoDB.get(params, function(err, data) {
            if (err) {
                console.error("Unable to get data for user " + userId, err);

                _response = buildOutput(500, err);
                return callback(_response, null);
            }
            else{
                console.log("Userinfo retrieved", data);
                _response = buildOutput(200, {
                    "user": data.Item
                });
                return callback(null, _response);
            }
        });
    }
    else if (event.resource === '/user' && event.httpMethod === "DELETE") {
        let userId = event.queryStringParameters.userId;

        console.log("Deleted user " + userId);
        _response = buildOutput(200, {
            "status": "OK"
        });
        return callback(null, _response);
    }
    else if (event.resource === '/user/info' && event.httpMethod === "POST") {
        let userId = event.queryStringParameters.userId;
        let interests = event.queryStringParameters.interests;
        let interestsArr = typeof interests !== 'undefined' ? interests.split('_') : [];
        let department = event.queryStringParameters.department;
        let dob = Number.parseInt(event.queryStringParameters.dob);
        let userName = event.queryStringParameters.userName;
        let fb = event.queryStringParameters.fbEmail;
        let notificationToken = event.queryStringParameters.notificationToken;
        let isWalkthroughShown = event.queryStringParameters.isWalkthroughShown;
        
        if (isWalkthroughShown) {
            isWalkthroughShown = isWalkthroughShown === 'true';
        }

        console.log("User interests", interestsArr);
        console.log("User department", department);
        console.log("User dob", dob);
        console.log("User name", userName);
        console.log("User fb", fb);
        console.log("User notificationToken", notificationToken);

        let params = {
            TableName: "UserInfo",
            Key: {
                "UserID": userId
            }
        };

        dynamoDB.get(params, function(err, data) {
            if (err) {
                console.error("Unable to get data for user " + userId, err);

                _response = buildOutput(500, err);
                return callback(_response, null);
            }
            else{
                console.log("Userinfo retrieved", data);

                data.Item.interests = interestsArr.length > 0 ? interestsArr : (typeof data.Item.interests === 'undefined' ? [] : data.Item.interests);
                data.Item.department = typeof department !== 'undefined' ? department : (typeof data.Item.department === 'undefined' ? '*' : data.Item.department);
                data.Item.dob = !Number.isNaN(dob) ? dob : (typeof data.Item.dob === 'undefined' ? new Date().getTime() : data.Item.dob);
                data.Item.name = typeof userName !== 'undefined' ? userName : data.Item.name;
                data.Item.fbEmail = typeof fb !== 'undefined' ? fb : (typeof data.Item.fbEmail === 'undefined' ? '*' : data.Item.fbEmail);
                data.Item.isWalkthroughShown = typeof isWalkthroughShown !== 'undefined' ? isWalkthroughShown : (typeof data.Item.isWalkthroughShown === 'undefined' ? false : data.Item.isWalkthroughShown);

                let newNotificationToken = typeof notificationToken !== 'undefined' ? notificationToken : (typeof data.Item.notificationToken === 'undefined' ? '*' : data.Item.notificationToken);
                
                if (newNotificationToken !== '*' && newNotificationToken !== data.Item.notificationToken) {
                    data.Item.notificationToken = newNotificationToken;

                    //SNS does not support endpoint creation in eu-west-2
                    AWS.config.update({region: 'eu-west-1'});
                    sns = new AWS.SNS({apiVersion: '2010-03-31'});

                    let paramsSNS = {
                      PlatformApplicationArn: 'arn:aws:sns:eu-west-1:623808306110:app/APNS/Bebolt-prod', /* required */
                      Token: newNotificationToken, /* required */
                      Attributes: {},
                      CustomUserData: ''
                    };
                    sns.createPlatformEndpoint(paramsSNS, function(err, res) {
                        if (err) {
                            console.log(err, err.stack); // an error occurred
                        }
                        else {
                            console.log("Platform endpoint creation success", res);
                            data.Item.endpointARN = res.EndpointArn;
                        }

                        params.Item = data.Item;

                        //Reset AWS services to eu-west-2
                        AWS.config.update({region: 'eu-west-2'});

                        dynamoDB.put(params, function(err, data) {
                            if (err) {
                                console.log("Error updating user information", err);
                                _response = buildOutput(500, err);
                                return callback(_response, null);
                            } else {
                                console.log("Successfully updated user information", data);
                                _response = buildOutput(200, {
                                    "status": "OK"
                                });
                                return callback(null, _response);
                            }
                        });
                    });
                }
                else {
                    params.Item = data.Item;

                    dynamoDB.put(params, function(err, data) {
                        if (err) {
                            console.log("Error updating user information", err);
                            _response = buildOutput(500, err);
                            return callback(_response, null);
                        } else {
                            console.log("Successfully updated user information", data);
                            _response = buildOutput(200, {
                                "status": "OK"
                            });
                            return callback(null, _response);
                        }
                    });
                }
            }
        });
    }
    else if (event.resource === '/users' && event.httpMethod === "GET") {
        let userId = event.queryStringParameters.userId;
        let userFriends = [];

        let params = {
            TableName: "UserInfo"
        };

        dynamoDB.scan(params, onScan);
        let users = [];

        function onScan(err, data) {
            if (err) {
                console.error("Unable to scan the table. Error JSON:", JSON.stringify(err, null, 2));
                
                _response = buildOutput(500, err);
                return callback(_response, null);
            }
            else {
                // print all the users
                console.log("Scan succeeded.");
                data.Items.forEach(function(result) {
                    if (result.UserID == userId){
                        userFriends = result.friends;
                    }
                    else{
                        users.push({id: result.UserID, name: result.name});
                    }
                });

                // continue scanning if we have more users, because
                // scan can retrieve a maximum of 1MB of data
                if (typeof data.LastEvaluatedKey != "undefined") {
                    console.log("Scanning for more...");
                    params.ExclusiveStartKey = data.LastEvaluatedKey;
                    dynamoDB.scan(params, onScan);
                }
                else {
                    let userNewFriends = [];
                    users.forEach(function(item) {
                        let isFriend = false;
                        for (let i = 0; i < userFriends.length; i++){
                            if (userFriends[i].id == item.id && userFriends[i].name == item.name){
                                isFriend = true;
                                break;
                            }
                        }

                        if (!isFriend) {
                            userNewFriends.push(item);
                        }
                    });

                    _response = buildOutput(200, {
                        "users": userNewFriends
                    });
                    return callback(null, _response);
                }
            }
        }
    }
    else if (event.resource === '/friends' && event.httpMethod === "GET") {
        let userId = event.queryStringParameters.userId;
        let friends = [];

        let params = {
            TableName: "UserInfo",
            Key: {
                "UserID": userId
            }
        };

        dynamoDB.get(params, function(err, data) {
            if (err) {
                console.error("Unable to get data for user " + userId, err);

                _response = buildOutput(500, err);
                return callback(_response, null);
            }
            else{
                console.log("Userinfo retrieved", data);

                data.Item.friends.forEach(function(friend) {
                    friends.push({id: friend.id, name: friend.name})
                });

                _response = buildOutput(200, {
                    "users": friends
                });
                return callback(null, _response);
            }
        });
    }
    else if (event.resource === '/friends' && event.httpMethod === "POST") {
        let userId = event.queryStringParameters.userId;
        let userName = event.queryStringParameters.userName;
        let friendId = event.queryStringParameters.friendId;
        let friendName = event.queryStringParameters.friendName;
        
        let notification = {
            name: userName + " wants to be your friend",
            time: new Date().getTime(),
            status: "unread",
            read: false,
            id: uuid(),
            to: {
                id: friendId,
                name: friendName
            },
            from: {
                id: userId,
                name: userName
            }
        };

        //Add user to friend's friends list
        let params = {
            TableName: "UserInfo",
            Key: {
                "UserID": friendId
            },
            UpdateExpression: 'set #notifications = list_append(#notifications, :newNotification)',
            ExpressionAttributeNames: {
                '#notifications': 'notifications'
            },
            ExpressionAttributeValues: {
                ':newNotification': [notification]
            }
        };

        dynamoDB.update(params, function(err, data) {
            if (err) {
                console.log("Error adding new friend", err);
                _response = buildOutput(500, err);
                return callback(_response, null);
            }
            else {
                console.log("Added new friend " + userId + " for " + friendId);

                params = {
                    TableName: "UserInfo",
                    Key: {
                        "UserID": friendId
                    }
                };

                dynamoDB.get(params, function(err, data) {
                    if (err) {
                        console.error("Unable to get data for user " + userId, err);

                        _response = buildOutput(500, err);
                        return callback(_response, null);
                    }
                    else{
                        console.log("Userinfo retrieved", data);

                        //SNS does not support endpoint creation in eu-west-2
                        AWS.config.update({region: 'eu-west-1'});
                        sns = new AWS.SNS({apiVersion: '2010-03-31'});

                        let paramsSNS = {
                            TargetArn: data.Item.endpointARN,
                            MessageStructure: 'json', // so we can put in a custom payload and message
                            Message: JSON.stringify({
                                APNS: JSON.stringify({
                                    aps: {
                                        alert: {
                                            title : "Friend Request",
                                            body : "You have a new friend request",
                                            sound: "default",
                                            badge: 1
                                        }
                                    },
                                    //You can add your custom data here.
                                    type : "notification",
                                    "notification-id": notification.id
                                }),
                            }),
                        };

                        sns.publish(paramsSNS, function(err, res) {
                            if (err) {
                                console.log(err, err.stack); // an error occurred
                            }
                            else {
                                console.log("Sent push notification successfully", res);
                            }

                            AWS.config.update({region: 'eu-west-2'});

                            _response = buildOutput(200, {"status": 'OK'});
                            return callback(null, _response);
                        });
                    }
                });
            }
        });
    }
    else if (event.resource === '/friends/reject' && event.httpMethod === "POST") {
        let userId = event.queryStringParameters.userId;
        let notificationId = event.queryStringParameters.notificationId;

        let params = {
            TableName: "UserInfo",
            Key: {
                "UserID": userId
            }
        };

        dynamoDB.get(params, function(err, data) {
            if (err) {
                console.error("Unable to get data for user " + userId, err);

                _response = buildOutput(500, err);
                return callback(_response, null);
            }
            else{
                console.log("Userinfo retrieved", data);
                const userName = data.Item.name;
                let fromUserId;
                
                //Update notification status
                data.Item.notifications.forEach(function (notification) {
                    if (notification.id === notificationId){
                        notification.status = 'rejected';
                        notification.read = true;
                        fromUserId = notification.from.id;
                    }
                });

                params.Item = data.Item;

                // Call DynamoDB to add the item to the table
                dynamoDB.put(params, function(err, data) {
                    if (err) {
                        console.log("Error cancelling user activity invite", err);
                        _response = buildOutput(500, err);
                        return callback(_response, null);
                    } else {
                        console.log("Successfully canceled activity invite", data);

                        let notification = {
                            name: userName + " rejected your friend request",
                            time: new Date().getTime(),
                            status: "rejected",
                            read: false,
                            id: uuid(),
                            from: {
                                id: userId,
                                name: userName
                            }
                        };

                        //Add friend to user friends list
                        let params = {
                            TableName: "UserInfo",
                            Key: {
                                "UserID": fromUserId
                            },
                            UpdateExpression: 'set #notifications = list_append(#notifications, :newNotification)',
                            ExpressionAttributeNames: {
                                '#notifications': 'notifications'
                            },
                            ExpressionAttributeValues: {
                                ':newNotification': [notification]
                            }
                        };

                        dynamoDB.update(params, function(err, data) {
                            if (err) {
                                console.log("Error rejecting new friend", err);
                                _response = buildOutput(500, err);
                                return callback(null, _response);
                            }
                            else {
                                console.log("Rejected new friend " + userName + " for " + fromUserId);

                                //SNS does not support endpoint creation in eu-west-2
                                AWS.config.update({region: 'eu-west-1'});
                                sns = new AWS.SNS({apiVersion: '2010-03-31'});

                                let paramsSNS = {
                                    TargetArn: data.Item.endpointARN,
                                    MessageStructure: 'json', // so we can put in a custom payload and message
                                    Message: JSON.stringify({
                                        APNS: JSON.stringify({
                                            aps: {
                                                alert: {
                                                    title : "Friend Request",
                                                    body : notification.name,
                                                    sound: "default",
                                                    badge: 1
                                                }
                                            },
                                            //You can add your custom data here.
                                            type : "notification",
                                            "notification-id": notification.id
                                        }),
                                    }),
                                };

                                sns.publish(paramsSNS, function(err, res) {
                                    if (err) {
                                        console.log(err, err.stack); // an error occurred
                                    }
                                    else {
                                        console.log("Sent push notification successfully", res);
                                    }

                                    AWS.config.update({region: 'eu-west-2'});

                                    _response = buildOutput(200, {"status": 'OK'});
                                    return callback(null, _response);
                                });
                            }
                        });
                    }
                });
            }
        });
    }
    else if (event.resource === '/friends/accept' && event.httpMethod === "POST") {
        let userId = event.queryStringParameters.userId;
        let notificationId = event.queryStringParameters.notificationId;

        let params = {
            TableName: "UserInfo",
            Key: {
                "UserID": userId
            }
        };

        dynamoDB.get(params, function(err, data) {
            if (err) {
                console.error("Unable to get data for user " + userId, err);

                _response = buildOutput(500, err);
                return callback(null, _response);
            }
            else{
                console.log("Userinfo retrieved", data);
                let newFriend = {
                    id: userId,
                    name: data.Item.name
                };
                const userName = data.Item.name;

                let fromUserId;

                //Update notification status
                data.Item.notifications.forEach(function (notification) {
                    if (notification.id === notificationId){
                        notification.status = 'accepted';
                        notification.read = true;
                        fromUserId = notification.from.id;
                        data.Item.friends.push({
                            id: notification.from.id,
                            name: notification.from.name
                        });
                    }
                });

                params.Item = data.Item;

                 // Call DynamoDB to add the item to the table
                 dynamoDB.put(params, function(err, data) {
                    if (err) {
                        console.log("Error accepting user friend request", err);
                        _response = buildOutput(500, err);
                        return callback(null, _response);
                    } else {
                        console.log("Successfully accepted friend request", data);

                        let notification = {
                            name: newFriend.name + " accepted your friend request",
                            time: new Date().getTime(),
                            status: "accepted",
                            read: false,
                            id: uuid(),
                            from: {
                                id: userId,
                                name: userName
                            }
                        };

                        //Add friend to user friends list
                        let params = {
                            TableName: "UserInfo",
                            Key: {
                                "UserID": fromUserId
                            },
                            UpdateExpression: 'set #friends = list_append(#friends, :newFriend), #notifications = list_append(#notifications, :newNotification)',
                            ExpressionAttributeNames: {
                                '#friends': 'friends',
                                '#notifications': 'notifications'
                            },
                            ExpressionAttributeValues: {
                                ':newFriend': [newFriend],
                                ':newNotification': [notification]
                            }
                        };

                        dynamoDB.update(params, function(err, data) {
                            if (err) {
                                console.log("Error adding new friend", err);
                                _response = buildOutput(500, err);
                                return callback(null, _response);
                            }
                            else {
                                console.log("Added new friend " + newFriend.id + " for " + fromUserId);

                                //SNS does not support endpoint creation in eu-west-2
                                AWS.config.update({region: 'eu-west-1'});
                                sns = new AWS.SNS({apiVersion: '2010-03-31'});

                                let paramsSNS = {
                                    TargetArn: data.Item.endpointARN,
                                    MessageStructure: 'json', // so we can put in a custom payload and message
                                    Message: JSON.stringify({
                                        APNS: JSON.stringify({
                                            aps: {
                                                alert: {
                                                    title : "Friend Request",
                                                    body : notification.name,
                                                    sound: "default",
                                                    badge: 1
                                                }
                                            },
                                            //You can add your custom data here.
                                            type : "notification",
                                            "notification-id": notification.id
                                        }),
                                    }),
                                };

                                sns.publish(paramsSNS, function(err, res) {
                                    if (err) {
                                        console.log(err, err.stack); // an error occurred
                                    }
                                    else {
                                        console.log("Sent push notification successfully", res);
                                    }

                                    AWS.config.update({region: 'eu-west-2'});

                                    _response = buildOutput(200, {"status": 'OK'});
                                    return callback(null, _response);
                                });
                            }
                        });
                    }
                });
            }
        });
    }
    else if (event.resource === '/friends/unfriend' && event.httpMethod === "POST") {
        let userId = event.queryStringParameters.userId;
        let friendId = event.queryStringParameters.friendId;

        let params = {
            TableName: "UserInfo",
            Key: {
                "UserID": userId
            }
        };

        dynamoDB.get(params, function(err, data) {
            if (err) {
                console.error("Unable to get data for user " + userId, err);

                _response = buildOutput(500, err);
                return callback(_response, null);
            }
            else{
                console.log("Userinfo retrieved", data);
                let newFriends = [];

                //Update notification status
                data.Item.friends.forEach(function (friend) {
                    if (friend.id !== friendId){
                        newFriends.push(friend);
                    }
                });

                data.Item.friends = newFriends;

                params.Item = data.Item;

                // Call DynamoDB to add the item to the table
                dynamoDB.put(params, function(err, data) {
                    if (err) {
                        console.log("Error unfriending user " + friendId, err);
                        _response = buildOutput(500, err);
                        return callback(_response, null);
                    } else {
                        console.log("Successfully unfriended user " + friendId, data);

                        params = {
                            TableName: "UserInfo",
                            Key: {
                                "UserID": friendId
                            }
                        };

                        dynamoDB.get(params, function(err, data) {
                            if (err) {
                                console.error("Unable to get data for user " + userId, err);

                                _response = buildOutput(500, err);
                                return callback(_response, null);
                            }
                            else{
                                console.log("Userinfo retrieved", data);
                                let newFriends = [];

                                //Update notification status
                                data.Item.friends.forEach(function (friend) {
                                    if (friend.id !== userId){
                                        newFriends.push(friend);
                                    }
                                });

                                data.Item.friends = newFriends;

                                params.Item = data.Item;

                                // Call DynamoDB to add the item to the table
                                dynamoDB.put(params, function(err, data) {
                                    if (err) {
                                        console.log("Error unfriending user " + userId, err);
                                        _response = buildOutput(500, err);
                                        return callback(_response, null);
                                    } else {
                                        console.log("Successfully unfriended user " + userId, data);

                                        _response = buildOutput(200, {
                                            "status": "OK"
                                        });
                                        return callback(null, _response);
                                    }
                                });
                            }
                        });
                    }
                });
            }
        });
    }
    else if (event.resource === '/notifications' && event.httpMethod === "GET") {
        let userId = event.queryStringParameters.userId;

        let params = {
            TableName: "UserInfo",
            Key: {
                "UserID": userId
            }
        };

        dynamoDB.get(params, function(err, data) {
            if (err) {
                console.error("Unable to get data for user " + userId, err);

                _response = buildOutput(500, err);
                return callback(_response, null);
            }
            else{
                console.log("Userinfo retrieved", data);

                let userNotifications = [];

                data.Item.notifications.forEach(function (notification) {
                    let newNotification = {
                        "id": notification.id,
                        "name": notification.name,
                        "status": notification.status,
                        "read": notification.read,
                        "timestamp": notification.time,
                        "from": notification.from
                    }

                    userNotifications.push(newNotification);
                });
                
                _response = buildOutput(200, {
                    "notifications": userNotifications.reverse()
                });
                return callback(null, _response);
            }
        });
    }
    else if (event.resource === '/notifications/read' && event.httpMethod === "POST") {
        let userId = event.queryStringParameters.userId;
        let notifications = event.queryStringParameters.notifications;
        const notificationsArr = notifications.split(',');

        let params = {
            TableName: "UserInfo",
            Key: {
                "UserID": userId
            }
        };

        dynamoDB.get(params, function(err, data) {
            if (err) {
                console.error("Unable to get data for user " + userId, err);

                _response = buildOutput(500, err);
                return callback(_response, null);
            }
            else{
                console.log("Userinfo retrieved", data);

                notificationsArr.forEach(function(notification) {
                    for (let i = 0; i < data.Item.notifications.length; i++){
                        if (notification === data.Item.notifications[i].id) {
                            data.Item.notifications[i].read = true;
                            break;
                        }
                    }
                })

                params.Item = data.Item;

                dynamoDB.put(params, function(err, data) {
                    if (err) {
                        console.log("Error updating user notifications" + userId, err);
                        _response = buildOutput(500, err);
                        return callback(_response, null);
                    } else {
                        console.log("Successfully updated user notifications" + userId, data);

                        _response = buildOutput(200, {
                            "status": "OK"
                        });
                        return callback(null, _response);
                    }
                });
            }
        });
    }
    else if (event.resource === '/pins' && event.httpMethod === "POST") {
        let userId = event.queryStringParameters.userId;
        let media = event.queryStringParameters.media;

        let params = {
            TableName: "UserInfo",
            Key: {
                "UserID": userId
            }
        };

        dynamoDB.get(params, function(err, data) {
            if (err) {
                console.error("Unable to get data for user " + userId, err);

                _response = buildOutput(500, err);
                return callback(null, _response);
            }
            else{
                console.log("Userinfo retrieved", data);

                const currentLocation = data.Item.activity.name.split(',')[0];

                let newPin = {
                    "ID": uuid(),
                    "location":{
                        "alt": Number.parseFloat(data.Item.activity.locations[0].alt),
                        "lat": Number.parseFloat(data.Item.activity.locations[0].lat),
                        "lng": Number.parseFloat(data.Item.activity.locations[0].lng),
                    },
                    "media": media,
                    "time": new Date().getTime()
                }

                newPin.activity = data.Item.activity.activity;
                newPin.name = data.Item.activity.name;
                newPin.participants = data.Item.activity.participants;
                newPin.atPlace = {
                    "location": data.Item.activity.locations[0],
                    "name": currentLocation
                };
                newPin.viewers = [];
                newPin.owner = {
                    "id": userId,
                    "name": data.Item.name
                };

                data.Item.activity = {};
                data.Item.activityInvited = [];

                params.Item = data.Item;

                 // Call DynamoDB to add the item to the table
                 dynamoDB.put(params, function(err, data) {
                    if (err) {
                        console.log("Error deleting user activity", err);
                        _response = buildOutput(500, err);
                        return callback(null, _response);
                    } else {
                        console.log("Successfully deleted user activity", data);

                        params = {
                            TableName: "Pins",
                            Key: {
                                "ID": currentLocation
                            }
                        }

                        dynamoDB.get(params, function(err, data) {
                            if (err) {
                                console.error("Unable to get data for pin location " + currentLocation, err);

                                _response = buildOutput(500, err);
                                return callback(null, _response);
                            }
                            else {

                                if (typeof data.Item === 'undefined') {
                                    data.Item = {};
                                    data.Item.ID = currentLocation;
                                    data.Item.pins = [];
                                }

                                data.Item.pins.push(newPin);

                                //Distribute pins equally in a circle
                                if (data.Item.pins.length > 1) {
                                    for (let i = 0; i < data.Item.pins.length; i++) {
                                        data.Item.pins[i].location.lat = data.Item.pins[i].atPlace.location.lat + 0.00005 * Math.cos(2 * Math.PI * i / 5); 
                                        data.Item.pins[i].location.lng = data.Item.pins[i].atPlace.location.lng + 0.00005 * Math.sin(2 * Math.PI * i / 5);
                                    }
                                }

                                params.Item =  data.Item;

                                console.log('Update object', params);

                                dynamoDB.put(params, function(err, data) {
                                    if (err) {
                                        console.log("Error creating new post", err);
                                        _response = buildOutput(500, err);
                                        return callback(null, _response);
                                    } else {
                                        console.log("Successfully created new post");
                                        _response = buildOutput(200, {
                                            "status": "OK"
                                        });
                                        return callback(null, _response);
                                    }
                                });
                            }
                        });
                    }
                });
            }
        });
    }
    else if (event.resource === '/pins' && event.httpMethod === "GET") {
        let userId = event.queryStringParameters.userId;
        console.log("Scanning Pins table.");
        
        let params = {
            TableName: "Pins"
        };

        dynamoDB.scan(params, onScan);
        let pins = [];

        function onScan(err, data) {
            if (err) {
                console.error("Unable to scan the table. Error JSON:", JSON.stringify(err, null, 2));
                
                _response = buildOutput(500, err);
                return callback(_response, null);
            }
            else {
                // print all the pins
                data.Items.forEach(function(result) {
                    pins = pins.concat(result.pins);
                });

                // continue scanning if we have more pins, because
                // scan can retrieve a maximum of 1MB of data
                if (typeof data.LastEvaluatedKey != "undefined") {
                    console.log("Scanning for more...");
                    params.ExclusiveStartKey = data.LastEvaluatedKey;
                    dynamoDB.scan(params, onScan);
                }
                else {
                    console.log("Pins", pins);
                    pins.forEach(function (pin) {
                        let seen = pin.owner.id === userId;
                        if (!seen) {
                            for (let i = 0; i < pin.viewers.length; i++) {
                                if (pin.viewers[i].id === userId) {
                                    seen = true;
                                    break;
                                }
                            }
                        }
                        pin.seen = seen;
                    });

                    _response = buildOutput(200, {
                        "pins": pins
                    });
                    return callback(null, _response);
                }
            }
        }
    }
    else if (event.resource === '/pin' && event.httpMethod === "GET") {
        let pinId = event.queryStringParameters.pinId;
        let place = event.queryStringParameters.place;

        let params = {
            TableName: "Pins",
            Key: {
                "ID": place
            }
        };

        dynamoDB.get(params, function(err, data) {
            if (err) {
                console.error("Unable to get pin " + pinId, JSON.stringify(err, null, 2));
                
                _response = buildOutput(500, err);
                return callback(null, _response);
            }
            else {
                // print all the pins
                console.log("Got pin data", data);

                let returnPin = {};

                for (let i = 0; i < data.Item.pins.length; i++) {
                    if (data.Item.pins[i].ID === pinId) {
                        returnPin = data.Item.pins[i];
                        break;
                    }
                }

                _response = buildOutput(200, {
                    "pin": returnPin
                });
                return callback(null, _response);
            }
        });
    }
    else if (event.resource === '/pin' && event.httpMethod === "DELETE") {
        let pinId = event.queryStringParameters.pinId;
        let place = event.queryStringParameters.place;

        let params = {
            TableName: "Pins",
            Key: {
                "ID": place
            }
        };

        dynamoDB.get(params, function(err, data) {
            if (err) {
                console.error("Unable to get pin " + pinId, JSON.stringify(err, null, 2));
                
                _response = buildOutput(500, err);
                return callback(null, _response);
            }
            else {
                // print all the pins
                console.log("Got pin data", data);

                let pinIndex;

                for (let i = 0; i < data.Item.pins.length; i++) {
                    if (data.Item.pins[i].ID === pinId) {
                        pinIndex = i;
                        break;
                    }
                }

                data.Item.pins.splice(pinIndex,1);

                params.Item = data.Item;

                dynamoDB.put(params, function(err, data) {
                    if (err) {
                        console.error("Unable to update remaining pins ", JSON.stringify(err, null, 2));
                        
                        _response = buildOutput(500, err);
                        return callback(null, _response);
                    }
                    else {
                        // print all the pins
                        console.log("Deleted pin successfully", data);
                        _response = buildOutput(200, {
                            "status": "OK"
                        });
                        return callback(null, _response);
                    }
                });
            }
        });
    }
    else if (event.resource === '/pins/view' && event.httpMethod === "POST") {
        let userId = event.queryStringParameters.userId;
        let userName = event.queryStringParameters.userName;
        let pinId = event.queryStringParameters.pinId;
        let place = event.queryStringParameters.place;

        let newView = {
            id: userId,
            name: userName
        }

        let params = {
            TableName: "Pins",
            Key: {
                "ID": place
            }
        };

        dynamoDB.get(params, function(err, data) {
            if (err) {
                console.error("Unable to get pin data " + pinId, err);

                _response = buildOutput(500, err);
                return callback(null, _response);
            }
            else {
                console.log("Pin data retrieved", data);
                let pinToView;

                for (let i = 0; i < data.Item.pins.length; i++) {
                    if (data.Item.pins[i].ID === pinId) {
                        pinToView = i;
                        break;
                    }
                }
                console.log("Owner " + data.Item.pins[pinToView].owner.id + " viewer " + userId);
                
                let newOne = !(data.Item.pins[pinToView].owner.id == userId);
                console.log("New one", newOne);
                
                if (newOne){
                    for (let i = 0; i < data.Item.pins[pinToView].viewers.length; i++){
                        if (data.Item.pins[pinToView].viewers[i].id == userId){
                            console.log("Match " + data.Item.pins[pinToView].viewers[i].id);
                            newOne = false;
                            break;
                        }
                    }
                }

                if (!newOne) {
                    console.log("Not a new view so ignoring");
                    _response = buildOutput(200, {
                        "status": "OK"
                    });
                    return callback(null, _response);
                }
                else {
                    data.Item.pins[pinToView].viewers.push(newView);
                    params.Item = data.Item;

                    dynamoDB.put(params, function(err, data) {
                        if (err) {
                            console.log("Error adding new view to post", err);
                            _response = buildOutput(500, err);
                            return callback(null, _response);
                        } else {
                            console.log("Successfully added new view to post");
                            _response = buildOutput(200, {
                                "status": "OK"
                            });
                            return callback(null, _response);
                        }
                    });
                }
            }
        });
    }
    else if (event.resource === '/invitecode/add' && event.httpMethod === "POST") {
        const inviteCode = event.queryStringParameters.code;
        console.log("New code", event.queryStringParameters.code);

        let params = {
            TableName: "UsefulInfo",
            Key: {
                "Key": "inviteCodes"
            }
        };

        dynamoDB.get(params, function(err, data) {
            if (err) {
                console.error("Unable to get inviteCodes", err);
            }
            else {
                console.log("Retrieved inviteCodes", data.Item);

                data.Item.Data[inviteCode] = inviteCode;
                params.Item = data.Item;

                dynamoDB.put(params, function(err, data) {
                        if (err) {
                            console.log("Error adding new inviteCode", err);
                        }
                        else {
                            console.log("Successfully added new inviteCode " + inviteCode);
                        }
                    });
            }
        });
    }
    else if (event.resource === '/invitecode/verify' && event.httpMethod === "POST") {
        const inviteCode = event.queryStringParameters.inviteCode;

        let params = {
            TableName: "UsefulInfo",
            Key: {
                "Key": "inviteCodes"
            }
        };

        dynamoDB.get(params, function(err, data) {
            if (err) {
                console.error("Unable to get inviteCodes", err);
                _response = buildOutput(500, err);
                return callback(null, _response);
            }
            else {
                console.log("Retrieved inviteCodes", data.Item);

                if (data.Item.Data[inviteCode]) {
                    console.log("Successfully verified inviteCode: " + inviteCode);
                    _response = buildOutput(200, {
                        "status": "OK"
                    });
                    return callback(null, _response);
                }
                else {
                    console.log("Error verifying the inviteCode: " + inviteCode);
                    _response = buildOutput(500, {
                        "Error": "Invalid invite code!"
                    });
                    return callback(null, _response);
                }
            }
        });
    }
    else if (event.resource === '/waitinglist/add' && event.httpMethod === "POST") {
        const userEmail = event.queryStringParameters.userEmail;

        let params = {
            TableName: "UsefulInfo",
            Key: {
                "Key": "waitingList"
            }
        };

        dynamoDB.get(params, function(err, data) {
            if (err) {
                console.error("Unable to get waitingList", err);
                _response = buildOutput(500, err);
                return callback(null, _response);
            }
            else {
                console.log("Retrieved waitingList", data.Item);

                if (data.Item.Data[userEmail]) {
                    console.log('Email ' + userEmail + ' already exists');
                    _response = buildOutput(200, {
                        "position": data.Item.Data[userEmail]
                    });
                    return callback(null, _response);
                }

                else {
                    data.Item.current = data.Item.current + 1;
                    data.Item.Data[userEmail] = data.Item.current;

                    const userPosition = data.Item.Data[userEmail];

                    params.Item = data.Item;

                    dynamoDB.put(params, function(err, data) {
                            if (err) {
                                console.log("Error adding new user to waitingList", err);
                                 _response = buildOutput(500, err);
                                return callback(null, _response);
                            }
                            else {
                                console.log('Added email ' + userEmail + ' on position ' + userPosition);
                                _response = buildOutput(200, {
                                    "position": userPosition
                                });
                                return callback(null, _response);
                            }
                        });
                }
            }
        });
    }
    else if (event.resource === '/waitinglist/position' && event.httpMethod === "GET") {
        const userEmail = event.queryStringParameters.userEmail;

        let params = {
            TableName: "UsefulInfo",
            Key: {
                "Key": "waitingList"
            }
        };

        dynamoDB.get(params, function(err, data) {
            if (err) {
                console.error("Unable to get waitingList", err);
                _response = buildOutput(500, err);
                return callback(null, _response);
            }
            else {
                console.log("Retrieved waitingList", data.Item);
                
                _response = buildOutput(200, {
                    "position": data.Item.Data[userEmail]
                });
                return callback(null, _response);
            }
        });
    }
    else if (event.resource === '/waitinglist/advance' && event.httpMethod === "POST") {
        const userEmail = event.queryStringParameters.userEmail;

        let params = {
            TableName: "UsefulInfo",
            Key: {
                "Key": "waitingList"
            }
        };

        dynamoDB.get(params, function(err, data) {
            if (err) {
                console.error("Unable to get waitingList", err);
                _response = buildOutput(500, err);
                return callback(null, _response);
            }
            else {
                console.log("Retrieved waitingList", data.Item);

                if (data.Item.Data[userEmail] > 1) {
                    data.Item.Data[userEmail] = data.Item.Data[userEmail] - 1;
                }

                const userPosition = data.Item.Data[userEmail];

                params.Item = data.Item;

                dynamoDB.put(params, function(err, data) {
                        if (err) {
                            console.log("Error adding new user to waitingList", err);
                             _response = buildOutput(500, err);
                            return callback(null, _response);
                        }
                        else {
                            console.log('Advanced email ' + userEmail + ' to position ' + userPosition);
                            _response = buildOutput(200, {
                                "position": userPosition
                            });
                            return callback(null, _response);
                        }
                    });
            }
        });
    }
    else if (event.resource === '/metadata' && event.httpMethod === "GET") {
        let zone = event.queryStringParameters.zone;

        let params = {
            TableName: "Metadata"
        }

        if (zone === 'central') {
            params.TableName += '-central';
        }

        console.log('Scanning ' + params.TableName);

        dynamoDB.scan(params, onScan).promise()
            .then(function(result) {
                console.log('Result', result);
                _response = buildOutput(200, {
                    "results": result
                });
                return callback(null, _response);
            })
            .catch(function(err) {
                console.log('Error', err);
                 _response = buildOutput(500, err);
                return callback(null, _response);
            });
    }
    else if (event.resource === '/token' && event.httpMethod === "POST") {
        let token = event.queryStringParameters.token;

        let params = {
          PlatformApplicationArn: 'arn:aws:sns:eu-west-1:623808306110:app/APNS/Bebolt-prod', /* required */
          Token: token, /* required */
          Attributes: {},
          CustomUserData: ''
        };
        sns.createPlatformEndpoint(params, function(err, data) {
            if (err) {
                console.log(err, err.stack); // an error occurred
                _response = buildOutput(500, err);
                return callback(null, _response);
            }
            else {
                console.log(data);           // successful response
                _response = buildOutput(200, {
                    "status": 'OK'
                });
                return callback(null, _response);
            }
        });
    }
    else {
        _response = buildOutput(500, invalid_path_err);
        return callback(_response, null);
    }
};


/* Utility functions */
let results = [];
function onScan(err, data) {
    if (err) {
        console.error("Unable to scan the table. Error JSON:", JSON.stringify(err, null, 2));
        return Promise.reject(err);
    } 
    else {
        // print all the movies
        console.log("Scan succeeded.");
        if (!results) {
            results = [];
        }
        data.Items.forEach(function(result) {
            results.push(result);
        });
 
        // continue scanning, because scan can retrieve a maximum of 1MB of data
        if (typeof data.LastEvaluatedKey != "undefined") {
            console.log("Scanning for more...");
            params.ExclusiveStartKey = data.LastEvaluatedKey;
            dynamoDB.scan(params, onScan);
        }
        else {
            //console.log("Places", places);
            return Promise.resolve(results);
        }
    }
}

// Build HTTP response for the microservices output
function buildOutput(statusCode, data) {
    let _response = {
        "statusCode": statusCode,
        "headers": {
            "Access-Control-Allow-Origin": "*",
            'content-type': 'application/json'
        },
        "body": JSON.stringify(data),
		"isBase64Encoded": false
    };
    return _response;
}


//userId=gs_ucl_ac_uk&userName=George Stoian&friendId=demo_ucl_ac_uk&activity=Go to Autogara Pub Isaccea, Best Cafelav, Best Cafelav and try one cocktail from each&activityAddress=Autogara Pub Isaccea, London&activityImage=ChIJd_clTbc4t0ARIgdRE0iaqfw&activityLocation=45.2745474,28.45807,0.0